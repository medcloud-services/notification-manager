package main

import (
	"log"

	"gitlab.com/medcloud-services/notification-manager/app"
)

// @title Swagger Notification API
// @version 1.0
// @description This is a server notification.
// @termsOfService http://swagger.io/terms/

// @contact.name Notification API Support (Specify support contacts: mail/url)

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /

func main() {
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
