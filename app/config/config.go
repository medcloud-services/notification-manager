package config

import (
	"log"
	"strings"
	"sync"

	"gitlab.com/medcloud-services/notification-manager/app/pkg/mailSender"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/rabbitMQ"

	"github.com/spf13/viper"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

type Config struct {
	Server struct {
		Host  string
		Port  string
		Mode  string
		Token string
	}
	Logger     zapLog.LoggerConf
	Rabbit     rabbitMQ.Config
	Database   postgre.Config
	MailSender mailSender.Config
}

var (
	config Config
	once   sync.Once
)

// Get reads config from environment and config.yml. Once.
func Get() *Config {
	once.Do(func() {
		viper.AddConfigPath("./public/config")
		viper.SetConfigName("config")

		viper.AllowEmptyEnv(true)
		viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
		viper.AutomaticEnv()

		// Read config.yml
		if err := viper.ReadInConfig(); err != nil {
			log.Fatal(err)
			return
		}

		if err := viper.Unmarshal(&config); err != nil {
			log.Fatal(err)
			return
		}
	})

	return &config
}
