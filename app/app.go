package app

import (
	"fmt"

	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/mailSender"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
	"go.uber.org/zap"
)

func Run() error {
	// Считываем настройки для этого сервера
	cnf := config.Get()

	// Инициализируем логгер
	zapLog.Init(cnf.Logger)
	defer func(Sug *zap.SugaredLogger) {
		err := Sug.Sync()
		if err != nil {
			_ = fmt.Errorf("nm -> Sug.Sync() -> error: %s", err.Error())
		}
	}(zapLog.Sug)

	zapLog.Sug.Infof("nm -> Логер успешно инициализирован")

	// Инициализация БД
	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}
	defer pg.Close()

	//  Запуск миграций
	ver, err := postgre.RunMigration()
	if err != nil {
		return err
	}
	zapLog.Sug.Infof("nm -> Migration done. Current schema version: %v ", ver)

	// Проверяем корректность конфигов нотификаций

	if err := mailSender.CheckConfig(&cnf.MailSender); err != nil {
		return err
	}

	// Запускаем слушатель обновления данных заказов через очереди
	if err := amqp.Init(); err != nil {
		return err
	}

	// Инициируем обработчики нотификаций

	if err := notification.Init(); err != nil {
		return err
	}

	// пример генерации pdf файлов
	/*err := pdfConvertor.ConvertExample(pdfConvertor.Example{Company: "Medcloud", Title: "Заголовок", Description: "Описание"})
	if err != nil {
		zapLog.Sug.Errorf("pdfConvertor err: %s", err)
	}*/

	// Запуск http сервера
	if err := http.Start(); err != nil {
		return err
	}

	return nil
}
