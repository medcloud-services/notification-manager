package mailSender

import (
	"strings"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"gopkg.in/gomail.v2"
)

type Sender struct {
	host string
	port int
	user string
	pass string
	mail *gomail.Message
}

func (s *Sender) NewMail() {
	s.mail = gomail.NewMessage()

	from := s.user
	if !strings.Contains(s.user, "@") {
		from = s.user + "@fake.ml"
	}
	s.mail.SetHeader("From", from)
}

func (s *Sender) SetSubject(subject string) {
	s.mail.SetHeader("Subject", subject)
}

func (s *Sender) SetBody(msg string) {
	s.mail.SetBody("text/html", msg)
}

func (s *Sender) AddFile(filePath []string) {
	for index := range filePath {
		s.mail.Attach(filePath[index])
	}
}

func (s *Sender) Send(sendTo []string) error {
	// указываем получателей
	s.mail.SetHeader("To", sendTo...)

	// отправляем письмо
	d := gomail.NewDialer(s.host, s.port, s.user, s.pass)
	if err := d.DialAndSend(s.mail); err != nil {
		zapLog.Sug.Errorf("mailSender -> DialAndSend. Ошибка отправки письма. Причина: %s", err)
		return err
	}

	return nil
}

func (s *Sender) setConfig(conf *Config) {
	s.host = conf.Host
	s.port = conf.Port
	s.user = conf.User
	s.pass = conf.Pass
}
