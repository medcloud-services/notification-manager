package mailSender

import (
	"errors"
)

type Config struct {
	Host string
	Port int
	User string
	Pass string
}

func Init(conf *Config) (*Sender, error) {
	if err := CheckConfig(conf); err != nil {
		return nil, err
	}

	sender := Sender{}
	sender.setConfig(conf)
	sender.NewMail()

	return &sender, nil
}

func CheckConfig(conf *Config) error {
	if conf.Host == "" {
		return errors.New("Sending mail is not available, host not set.  mailSender.host")
	}

	if conf.Port == 0 {
		return errors.New("Sending mail is not available, port not set.  mailSender.port")
	}

	if conf.User == "" {
		return errors.New("Sending mail is not available, user not set.  mailSender.user")
	}

	if conf.Pass == "" {
		return errors.New("Sending mail is not available, pass not set.  mailSender.pass")
	}

	return nil
}
