package rabbitMQ

type Config struct {
	Host string
	Port string
	User string
	Pass string
}

func Init(conf *Config) (*Rabbit, error) {
	rabbit := Rabbit{}
	err := rabbit.connect(conf.Host, conf.Port, conf.User, conf.Pass)
	if err != nil {
		return nil, err
	}

	return &rabbit, nil
}
