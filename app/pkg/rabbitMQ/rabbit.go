package rabbitMQ

import (
	"errors"
	"time"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/streadway/amqp"
)

type Rabbit struct {
	conn    *amqp.Connection
	channel *amqp.Channel
}

const exchangeDefault = "event"

// todo setter

func (r *Rabbit) connect(host, port, user, pass string) (err error) {
	err = checkConfig(host, port, user, pass)
	if err != nil {
		return err
	}

	connectionString := "amqp://" + user + ":" + pass + "@" + host + ":" + port

	r.conn, err = amqp.Dial(connectionString)
	if err != nil {
		return errors.New("Failed to connect to Rabbit. URL: " + connectionString)
	}

	return
}

func (r *Rabbit) IsConnExist() bool {
	return r.conn != nil
}

func (r *Rabbit) SendMsg(queueName string, msg []byte) (err error) {
	err = checkQueueName(queueName)
	if err != nil {
		return err
	}

	if r.conn == nil {
		return errors.New("Tried to send message before connection was initialized.  Don't do that")
	}

	ch, err := r.conn.Channel()
	defer func(ch *amqp.Channel) {
		err := ch.Close()
		if err != nil {
			zapLog.Sug.Warnf("amqp: ch.Close(): %s", err.Error())
		}
	}(ch)

	// Объявления очереди (идемпотентно)
	_, err = ch.QueueDeclare(
		queueName, // Наименование очереди (макс - 255 байт)
		false,     // Долговечная очередь?! true - очередь переживет перезапуск сервера.
		false,     // очередь удаляется, когда отписывается последний подписчик
		false,     // используется только одним соединением, и очередь будет удалена при закрытии соединения
		false,     // Не ждать подтверждение от сервера о успешном соединении
		nil,       // arguments
	)

	// Публикует сообщение в очередь.
	err = ch.Publish(
		"",        // "" - стандартный обменник. (Можно выбрать другой. Например: log)
		queueName, // Ключ очереди
		false,     // Обязательный: true - сообщение может быть потеряно
		false,     // Немедленно: true - сообщение доставляется вне очереди. Если нет готовых принять сообщение слушателей, сообщение будет потеряно.
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         msg, // []byte
		})

	return err
}

func (r *Rabbit) StartListener(queueName string, handlerFunc func(amqp.Delivery), description string, retryFunc func() bool) (err error) {
	err = checkQueueName(queueName)
	if err != nil {
		return err
	}

	r.channel, err = r.conn.Channel()
	if err != nil {
		return err
	}
	defer r.channel.Close()

	if err = r.channel.ExchangeDeclare(
		exchangeDefault, // name of the exchange
		"direct",        // type flag.String("exchange-type", "direct", "Exchange type - direct|fanout|topic|x-custom")
		false,           // durable
		false,           // delete when complete
		false,           // internal
		false,           // noWait
		nil,             // arguments
	); err != nil {
		return err
	}

	_, err = r.channel.QueueDeclare(
		queueName, // Наименование очереди (макс - 255 байт)
		false,     // Долговечная очередь?! true - очередь переживет перезапуск сервера.
		false,     // очередь удаляется, когда отписывается последний подписчик
		false,     // используется только одним соединением, и очередь будет удалена при закрытии соединения
		false,     // Не ждать подтверждение от сервера о успешном соединении
		map[string]interface{}{
			"Описание": description,
		}, // arguments
	)

	if err != nil {
		// Не совпадают параметры очередей.
		return err
	}

	key := queueName + exchangeDefault

	if err = r.channel.QueueBind(
		queueName,       // Наименование очереди (макс - 255 байт)
		key,             // bindingKey
		exchangeDefault, // sourceExchange
		false,           // noWait
		nil,             // arguments
	); err != nil {
		return err
	}

	err = r.channel.Qos(10, 0, false)
	if err != nil {
		return err
	}

	deliveries, err := r.channel.Consume(
		queueName,    // Наименование очереди (макс - 255 байт)
		"tagConsume", // Tag потребителя,
		false,        // auto-ack or noack
		false,        // exclusive
		false,        // noLocal
		false,        // noWait
		nil,          // arguments
	)
	if err != nil {
		return err
	}

	done := make(chan bool)

	go func() {
		msgCount := 0
		for d := range deliveries {
			msgCount++
			handlerFunc(d)

			if err := d.Ack(false); err != nil {
				zapLog.Sug.Warnf("handle: deliveries channel error: %s", err.Error())
			}
		}
		zapLog.Sug.Infof("handle: deliveries channel closed")
		retryConnection(retryFunc)
	}()
	<-done
	return nil
}

func (r *Rabbit) Close() {
	if r.conn != nil {
		_ = r.conn.Close()
	}
}

func checkConfig(host, port, user, pass string) error {
	if host == "" {
		return errors.New("Cannot initialize connection to Rabbit, host not set.  RABBIT_HOST")
	}

	if port == "" {
		return errors.New("Cannot initialize connection to Rabbit, port not set.  RABBIT_PORT")
	}

	if user == "" {
		return errors.New("Cannot initialize connection to Rabbit, user login not set.  RABBIT_USER")
	}

	if pass == "" {
		return errors.New("Cannot initialize connection to Rabbit, user password not set.  RABBIT_PASS")
	}
	return nil
}

func checkQueueName(queueName string) error {
	if len([]byte(queueName)) > 255 {
		return errors.New("Exceeded the maximum size of the queue name.  (255 bytes)")
	}
	return nil
}

func retryConnection(retryFunc func() bool) {
	ticker := time.NewTicker(time.Minute)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:

				if retryFunc() {
					<-quit
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
