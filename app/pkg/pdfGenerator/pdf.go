package pdfGenerator

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"os"
	"strconv"
	"time"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

// Path for download pdf
const (
	outputPath = "./public/tempPDF/"
	tempPath   = "public/cloneTemplate/"
)

// RequestPdf Pdf requestPdf struct
type RequestPdf struct {
	body string
}

// NewRequestPdf New request to pdf
func NewRequestPdf(body string) *RequestPdf {
	return &RequestPdf{
		body: body,
	}
}

// ParseTemplate Parsing template
func (r *RequestPdf) ParseTemplate(templateFileName string, data interface{}) error {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		zapLog.Sug.Errorf("RequestPdf -> template.ParseFiles:  %s", err)
		return err
	}

	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		zapLog.Sug.Errorf("RequestPdf -> Execute(buf, data):  err=%s,  buf=%s, data=%s", err, buf, data)
		return err
	}
	r.body = buf.String()

	return nil
}

// GeneratePDF Generate pdf
func (r *RequestPdf) GeneratePDF() (string, error) {
	newFileName := strconv.FormatInt(int64(time.Now().Unix()), 10)

	checkTempPath()

	err1 := ioutil.WriteFile(tempPath+newFileName+".html", []byte(r.body), 0o644)
	if err1 != nil {
		zapLog.Sug.Errorf("GeneratePDF -> Write temp File:  %s", err1)
		panic(err1)
	}

	f, err := os.Open(tempPath + newFileName + ".html")
	if err != nil {
		zapLog.Sug.Errorf("GeneratePDF -> Open temp File:  %s", err)
		return "", err
	}
	defer f.Close()

	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		zapLog.Sug.Errorf("GeneratePDF -> NewPDFGenerator:  %s", err)
		return "", err
	}

	pdfg.AddPage(wkhtmltopdf.NewPageReader(f))

	pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)

	pdfg.Dpi.Set(300)

	err = pdfg.Create()
	if err != nil {
		zapLog.Sug.Errorf("GeneratePDF -> pdfg.Create:  %s", err)
		return "", err
	}

	err = pdfg.WriteFile(outputPath + newFileName + ".pdf")
	if err != nil {
		zapLog.Sug.Errorf("GeneratePDF -> pdfg.WriteFile:  %s", err)
		return "", err
	}

	err = os.Remove(tempPath + newFileName + ".html")
	if err != nil {
		zapLog.Sug.Errorf("GeneratePDF -> Remove temp file:  %s", err)
	}

	return outputPath + newFileName + ".pdf", nil
}

func checkTempPath() {
	if _, err := os.Stat(tempPath); os.IsNotExist(err) {
		errDir := os.Mkdir(tempPath, 0o777)
		if errDir != nil {
			zapLog.Sug.Errorf("errDir -> checkTempPath:  %s", errDir)
			panic(errDir)
		}
	}
}
