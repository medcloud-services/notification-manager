package notification

import (
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification/mail"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func InitNotification(msg models.Msg) {
	n := getNotification(msg)
	err := n.Save()
	if err != nil {
		zapLog.Sug.Warnf("InitNotification -> Save methor return error: %s", err.Error())
	}
}

func getNotification(msg models.Msg) Notification {
	var n Notification

	switch msg.TypeMsg {
	case mail.TypeMsg:
		n = &mail.Mail{Msg: msg}
	default:
		n = &EmptyNotification{Msg: msg}
	}

	return n
}
