package notification

import (
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/mails"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/configs"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification/mail/periodicSender"
)

func Init() error {
	// получаем набор конфигов МИС из БД
	misConfigs := configs.GetAll()

	// пробегаемся по набору настроек и вызываем соответствующее количество обработчиков
	for _, conf := range misConfigs {

		// Запускаем наблюдатель за письмами готовыми к отправке.
		p := periodicSender.New(conf)
		p.Run()

		// Инициация повторной отправки сообщений, которые не ушли из-за отключения NM.
		_, _ = mails.UpdateStatusByStatusName("QUEUE_FOR_SENDING", "READY_FOR_SEND", conf.Mis)
	}

	return nil
}
