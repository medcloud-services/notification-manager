package notification

import (
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification/mail"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func InitResult(msg models.MsgResult) {
	zapLog.Sug.Infof("notification -> InitResult.")

	n := getNotificationResult(msg)
	err := n.Save()
	if err != nil {
		zapLog.Sug.Warnf("InitResult -> Save methor return error: %s", err.Error())
	}
}

func getNotificationResult(msg models.MsgResult) Notification {
	var n Notification

	switch msg.Type {
	case mail.TypeMsg:
		n = &mail.Result{Msg: msg}
	default:
		n = &EmptyNotification{Msg: msg}
	}

	return n
}
