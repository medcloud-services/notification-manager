package periodicSender

import (
	"fmt"
	"time"

	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification/mail"

	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/mails"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

// todo Глобальные переменные зло!
var senders = make(map[string]*sender)

const updateRate = 10

type sender struct {
	mis         string
	conf        models.Config
	stopWatch   chan bool
	stopSend    chan bool
	queueToSend mail.Queue
}

func New(misConf models.MisConfig) *sender {
	senders[misConf.Mis] = &sender{
		mis:  misConf.Mis,
		conf: misConf.Conf,
	}

	return senders[misConf.Mis]
}

func Get(misConf models.MisConfig) (*sender, error) {
	if senders[misConf.Mis] != nil {
		return senders[misConf.Mis], nil
	}

	return &sender{}, fmt.Errorf("Sender not found ")
}

func RestartAll() {
	for _, v := range senders {
		v.Restart()
	}
}

func (s *sender) SetConfig(conf models.Config) {
	s.conf = conf
}

func (s *sender) Run() {
	s.runWatcher()
	s.runSender()
}

func (s *sender) Restart() {
	zapLog.Sug.Info("periodicSender -> Restart -> mis:", s.mis)

	s.stopWatch <- true
	s.stopSend <- true

	s.Run()
}

// runWatcher Периодический мониторинг готовых к отправке писем
func (s *sender) runWatcher() {
	zapLog.Sug.Info("periodicSender -> Restart -> mis:", s.mis)

	ticker := time.NewTicker(updateRate * time.Second)
	s.stopWatch = make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				ids, _ := mails.UpdateStatusByStatusName("READY_FOR_SEND", "QUEUE_FOR_SENDING", s.mis)
				if len(ids) > 0 {
					zapLog.Sug.Info("periodicSender -> runWatcher -> Получил:", ids)

					s.queueToSend.Adds(ids)
				}
			case <-s.stopWatch:
				zapLog.Sug.Debug("periodicSender -> runWatcher -> stopWatch")
				ticker.Stop()
				return
			}
		}
	}()
}

// runSender отправщик периодических писем
func (s *sender) runSender() {
	zapLog.Sug.Info("periodicSender -> runSender -> time.Minute / time.Duration(s.conf.SendPerMinute): ", time.Minute/time.Duration(s.conf.SendPerMinute))

	ticker := time.NewTicker(time.Minute / time.Duration(s.conf.SendPerMinute))
	s.stopSend = make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				ids := s.queueToSend.Get()
				if len(ids) > 0 {
					sendId := ids[0]

					err := mail.Send(sendId, s.mis)

					s.queueToSend.RemoveFirst()

					if err != nil {
						//  возвращаем в конец очереди отправки.
						s.queueToSend.Add(sendId)

						// todo Продумать, что дополнительно нужно проделать при ошибке.
						// Сейчас сообщения отправляются в rabbit для дальнейшей отправки через мис.
					}
				}
			case <-s.stopSend:
				zapLog.Sug.Debug("periodicSender -> runSender -> stopSend")
				ticker.Stop()
				return
			}
		}
	}()
}
