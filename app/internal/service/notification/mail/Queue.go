package mail

import "sync"

type Queue struct {
	ids []int16
	sync.RWMutex
}

func (s *Queue) Add(ids int16) {
	s.Lock()
	defer s.Unlock()

	s.ids = append(s.ids, ids)
}

func (s *Queue) Adds(ids []int16) {
	s.Lock()
	defer s.Unlock()

	s.ids = append(s.ids, ids...)
}

func (s *Queue) Get() []int16 {
	s.RLock()
	defer s.RUnlock()

	return s.ids
}

func (s *Queue) RemoveFirst() {
	s.Lock()
	defer s.Unlock()

	s.ids = s.ids[1:]
}
