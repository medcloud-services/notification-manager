package mail

import (
	"encoding/json"
	"errors"

	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/mails"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/patterns"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/configs"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/rabbitMQ"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

const (
	TypeMsg          = "Email"
	ReadyToSendQueue = "ReadyToSend"
)

type Mail struct {
	Msg models.Msg
}

type Data struct {
	User              interface{}  `json:"user"`
	Order             interface{}  `json:"order"`
	ResultIds         []int        `json:"resultIds"`
	ProtocolData      ProtocolData `json:"protocolData"`
	MedicalCenter     interface{}  `json:"medicalCenter"`
	AdditionalActions interface{}  `json:"additionalActions"`
}

type ProtocolData struct {
	Ids              []int `json:"ids"`
	ShowHeaders      bool  `json:"showHeaders"`
	ForceSendFields  bool  `json:"forceSendFields"`
	FromEmailSending bool  `json:"fromEmailSending"`
}

func (p *Mail) Save() error {
	zapLog.Sug.Info("Mail -> Save. ", p.Msg.TypePattern)

	conf, _ := configs.Get(p.Msg.MisCode)

	// Получить PatternID
	patternID, _ := patterns.GetID(p.Msg.TypePattern)

	// Выставить статус
	statusName := "READY_FOR_SEND"
	if conf.ManualSendResult {
		statusName = "CONFIRMATION_AWAITING"
	}

	var dataNew Data
	if err := json.Unmarshal([]byte(p.Msg.Data), &dataNew); err != nil {
		return err
	}

	// Проверка на повторение нотификаций
	idOld, oldDataJson, err := mails.GetRepetitions(p.Msg.OrderCode, p.Msg.SendTo)
	if err == nil && idOld > 0 {
		zapLog.Sug.Debug("Mail -> Save. Нотификация уже существует. oldId: ", idOld)

		// Условный случай для удаления нотификации
		if len(dataNew.ResultIds) == 0 && len(dataNew.ProtocolData.Ids) == 0 {
			// данных нет.
			zapLog.Sug.Info("Mail -> Save. Удаление нотификации. oldId: ", idOld)

			return mails.RemoveMail(idOld)
		}

		// Условный случай для игнорирования объединения с существующей нотификация. Создать новую
		if dataNew.ProtocolData.FromEmailSending {
			zapLog.Sug.Debugf("Mail -> Save. Объединение нотификаций ")

			var dataOld Data
			if err := json.Unmarshal([]byte(oldDataJson), &dataOld); err != nil {
				return err
			}

			dataNew.ProtocolData.Ids = unique(append(dataNew.ProtocolData.Ids, dataOld.ProtocolData.Ids...))

			// Обновить информацию о нотификации
			if err := mails.UpdateData(idOld, dataNew); err != nil {
				return err
			}
			return nil
		}

	}

	// Условный случай: пустые данные, нотификацию не создаем
	if len(dataNew.ResultIds) == 0 && len(dataNew.ProtocolData.Ids) == 0 {
		zapLog.Sug.Debugf("Mail -> Save. Отклонение нотификации. len(dataNew.ResultIds) == 0 ")

		return nil
	}

	zapLog.Sug.Debugf("Mail -> Save. Сохраняем новую нотификацию")

	// Сохранить данные нотификации
	dataID, _ := mails.SetNewData(p.Msg.MisCode, p.Msg.KeyCode, p.Msg.OrderCode, p.Msg.Data, patternID)
	zapLog.Sug.Debug("Mail -> Save. dataID:", dataID, " statusName: ", statusName)

	// Сохранить информацию о нотификации
	err = mails.SetNewInfo(dataID, statusName, p.Msg.SendTo)
	if err != nil {
		return err
	}

	return nil
}

func Send(idInfo int16, misCode string) error {
	cnf := config.Get()

	// получить данные для письма
	res, _ := mails.GetMailByInfo(idInfo)

	// отправить в очередь. // todo нормально ли, что для каждого сообщения новое соединение???
	rabbit, _ := rabbitMQ.Init(&cnf.Rabbit)
	if rabbit.IsConnExist() {

		jsonRes, err := json.Marshal(res)
		if err != nil {
			zapLog.Sug.Warnf("mail -> Send -> json.Marshal: Ошибка конверирования в json. Причина: %s", err)
			return err
		}

		err = rabbit.SendMsg(ReadyToSendQueue+"-"+misCode, jsonRes)
		if err != nil {
			zapLog.Sug.Warnf("mail -> Send -> rabbit.SendMsg: Ошибка отправки в rabbit. Причина: %s", err)
			return err
		}

		_, _ = mails.UpdateStatusByStatusName("QUEUE_FOR_SENDING", "SENDING", misCode)

		// успешная отправка.
		return nil
	}

	return errors.New("There is no connection to Rabbit. ")
}

// todo попахивает...
func unique(intSlice []int) []int {
	keys := make(map[int]bool)
	list := []int{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
