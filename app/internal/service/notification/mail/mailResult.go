package mail

import (
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/mails"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

type Result struct {
	Msg models.MsgResult
}

func (r *Result) Save() (err error) {
	zapLog.Sug.Info("MsgResult -> Save. ", r.Msg.Status, r.Msg.Id, r.Msg.ErrorMsg)

	err = mails.SaveSendResult(r.Msg.Id, r.Msg.Status, r.Msg.ErrorMsg)
	if err != nil {
		return
	}
	return
}
