package notification

import zapLog "gitlab.com/medcloud-services/support/zap-logger"

type Notification interface {
	Save() error
}

type EmptyNotification struct {
	Msg interface{}
}

func (p *EmptyNotification) Save() error {
	zapLog.Sug.Warn("Method not implemented. Data: ", p.Msg)
	return nil
}
