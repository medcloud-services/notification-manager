package pdfConvertor

import (
	pdf "gitlab.com/medcloud-services/notification-manager/app/pkg/pdfGenerator"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

type Example struct {
	Title       string
	Description string
	Company     string
	Contact     string
	Country     string
	City        string
}

// html template path
var templatePath = "./public/templates/example.html"

func ConvertExample(data Example) error {
	requestPdf := pdf.NewRequestPdf("")

	err := requestPdf.ParseTemplate(templatePath, data)
	if err == nil {
		pathToPdf, errSave := requestPdf.GeneratePDF()

		if errSave == nil {
			zapLog.Sug.Infof("Path to new pdf:  %s", pathToPdf)
		}

		return errSave
	}

	return err
}
