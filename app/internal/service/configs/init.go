package configs

import (
	"fmt"
	"sync"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/jackc/pgx/v4"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/config"
)

var (
	configs map[string]models.MisConfig
	once    sync.Once
)

func GetAll() map[string]models.MisConfig {
	once.Do(func() {
		configs, _ = config.GetAll()
	})

	return configs
}

func Set(misConf models.MisConfig) error {
	zapLog.Sug.Info("configs -> Set -> mis:", misConf.Mis)

	if err := config.Set(misConf.Conf, misConf.Mis); err != nil {
		return fmt.Errorf("config.Set err: %s", err.Error())
	}

	configs[misConf.Mis] = misConf

	return nil
}

func Get(misCode string) (models.Config, error) {
	_, v := configs[misCode]
	if v {
		return configs[misCode].Conf, nil
	}

	conf, err := config.Get(misCode)

	if err == pgx.ErrNoRows {
		return conf, ErrConfigNotFound
	}

	return conf, err
}

func Create(misCode string) (models.Config, error) {
	zapLog.Sug.Infof("configs -> Create: Create defaul config for misCode: %s", misCode)

	conf, err := config.CreateNew(misCode)
	if err != nil {
		return models.Config{}, err
	}

	configs[misCode] = models.MisConfig{Mis: misCode, Conf: conf}

	return conf, nil
}
