package configs

type Config struct{}

func (c *Config) LoadConfig() {
}

func (c *Config) Update(configJson string) {
}

func (c *Config) Get() {
}
