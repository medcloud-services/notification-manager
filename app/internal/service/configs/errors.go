package configs

import "errors"

var ErrConfigNotFound = errors.New("Config not found ")
