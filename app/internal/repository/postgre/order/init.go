package order

import (
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func Save(order models.OrderInput) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"select * from create_or_update_data_order($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)",
		order.Id, order.Code, order.Name, order.Surname, postgre.NewNullString(order.Birthday), order.Patronymic, order.MedCenterId,
		order.State, postgre.NewNullString(order.DoneTime), order.PaymentTypeId, postgre.NewNullString(order.PaymentTypeChangeTime))

	var isExist interface{}

	err = row.Scan(&isExist)
	if err != nil {
		zapLog.Sug.Warnf("order -> Save -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}
