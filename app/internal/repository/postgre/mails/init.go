package mails

import (
	"github.com/lib/pq"
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/statuses"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
)

// todo Логирование sql запросов: https://stackoverflow.com/questions/64504879/how-to-log-queries-with-pgx

type mailData struct {
	Id        int
	SendTo    []string
	OrderInfo models.OrderOutput
	MailData  interface{}
	CreateAt  pgtype.Timestamp
	DateSend  pgtype.Timestamp
	Status    string
	Error     string
}

type mailInfo struct {
	InfoID      int      `json:"InfoID"`
	SendTo      []string `json:"SendTo"`
	MailData    string   `json:"MailData"`
	PatternName string   `json:"PatternName"`
	PatternData string   `json:"PatternData"`
}

// GetMails Получение набора писем
func GetMails(filter models.MailFilter, misCode string) (res []interface{}, err error) {
	zapLog.Sug.Debug("mails -> GetMails -> filter: ", filter)
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	rows, err := pg.Pool.Query(pg.Context, "select * from get_mails($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
		filter.CreateDate, filter.DoneDate, filter.PayedDate, filter.FullName, filter.OrderNumber, filter.Recipient,
		filter.Status, misCode, filter.Limit, filter.Offset)
	if err != nil {
		zapLog.Sug.Errorf("mails -> GetMails -> conn.Query failed. Причина: %s", err)
		return
	}
	defer rows.Close()

	res = []interface{}{}

	for rows.Next() {
		var mail mailData
		var orderInfoCont models.OrderDB

		err = rows.Scan(&mail.Id, &mail.MailData, &mail.SendTo, &mail.CreateAt, &mail.DateSend, &mail.Status,
			&orderInfoCont, &mail.Error)
		if err != nil {
			zapLog.Sug.Warnf("mails -> GetMails -> rows.Scan failed. Причина: %s", err)
		}

		combine(&mail.OrderInfo, orderInfoCont)

		res = append(res, mail)
	}

	return
}

// UpdateStatusByStatusName Обновить статус по названию. Возвращает набор id (mails_info), затронутых обновлением
func UpdateStatusByStatusName(oldStatus, newStatus, misCode string) (res []int16, err error) {
	// Получить Status ID
	oldStatusID, _ := statuses.GetIdByName(oldStatus)
	newStatusID, _ := statuses.GetIdByName(newStatus)

	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	rows, err := pg.Pool.Query(pg.Context,
		"UPDATE mails_info i "+
			"SET status_id = $1 "+
			"WHERE i.msg_id in (Select id FROM mails_data WHERE mis_code = $2) "+
			"AND status_id = $3 returning id;", newStatusID, misCode, oldStatusID)
	if err != nil {
		zapLog.Sug.Errorf("mails -> UpdateStatusByStatusName -> conn.Query failed. Причина: %s", err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var idInfo int16

		err = rows.Scan(&idInfo)
		if err != nil {
			zapLog.Sug.Warnf("mails -> UpdateStatusByStatusName -> rows.Scan failed. Причина: %s", err)
		}

		res = append(res, idInfo)
	}

	return
}

// UpdateStatusByID Обновить статус по ключ-id статуса.
func UpdateStatusByID(ids []int, newStatusName, misCode string) (res []int16, err error) {
	idArr := &pgtype.Int4Array{}
	_ = idArr.Set(ids)
	newStatusID, _ := statuses.GetIdByName(newStatusName)

	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	rows, err := pg.Pool.Query(pg.Context,
		"UPDATE mails_info i "+
			"SET status_id = $1 "+
			"WHERE i.msg_id = ANY ($2) "+
			"AND i.msg_id in (Select id FROM mails_data WHERE mis_code = $3) returning id;", newStatusID, idArr, misCode)
	if err != nil {
		zapLog.Sug.Errorf("mails -> UpdateStatusByStatusName -> conn.Query failed. Причина: %s", err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var idInfo int16

		err := rows.Scan(&idInfo)
		if err != nil {
			zapLog.Sug.Warnf("mails -> UpdateStatusByStatusName -> rows.Scan failed. Причина: %s", err)
		}

		res = append(res, idInfo)
	}

	return
}

// GetAllMailsByInfo возвращает набор писем (data+info)
func GetAllMailsByInfo(idInfo []int16) (res map[int]interface{}, err error) {
	idArr := &pgtype.Int4Array{}
	_ = idArr.Set(idInfo)

	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	rows, err := pg.Pool.Query(pg.Context,
		"SELECT i.id, i.send_to, d.data, p.name, p.data FROM mails_info i "+
			"LEFT JOIN mails_data d ON i.msg_id = d.id "+
			"LEFT JOIN patterns p on d.pattern_id = p.id  "+
			"WHERE i.id = ANY ($1)",
		idArr)
	if err != nil {
		zapLog.Sug.Errorf("mails -> GetAllMailsByInfo -> conn.Query failed. Причина: %s", err)
		return
	}
	defer rows.Close()

	i := 0
	res = make(map[int]interface{})
	for rows.Next() {
		var mail struct {
			InfoID      int
			SendTo      string
			MailData    string
			PatternName string
			PatternData string
		}

		err = rows.Scan(&mail.InfoID, &mail.SendTo, &mail.MailData, &mail.PatternName, &mail.PatternData)
		if err != nil {
			zapLog.Sug.Warnf("mails -> GetAllMailsByInfo -> rows.Scan failed. Причина: %s", err)
		}

		res[i] = mail
	}

	return
}

// GetMailByInfo возвращает данные письма (data+info)
func GetMailByInfo(idInfo int16) (interface{}, error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return nil, err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"SELECT i.id, i.send_to, d.data, p.name, p.data FROM mails_info i "+
			"LEFT JOIN mails_data d ON i.msg_id = d.id "+
			"LEFT JOIN patterns p on d.pattern_id = p.id  "+
			"WHERE i.id = ($1)",
		idInfo)

	var mail mailInfo
	err = row.Scan(&mail.InfoID, &mail.SendTo, &mail.MailData, &mail.PatternName, &mail.PatternData)
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> GetAllMailsByInfo -> Ошибка запроса. Причина: %s", err)
		return nil, err
	}

	return mail, err
}

func SetNewData(misCode, keyCode, orderCode string, data interface{}, pattern int) (id int, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	row := pg.Pool.QueryRow(pg.Context,
		"INSERT INTO mails_data (mis_code, key_code, order_code, data, pattern_id) VALUES ($1, $2, $3, $4, $5) RETURNING id",
		misCode, keyCode, orderCode, data, pattern)

	err = row.Scan(&id)
	if err == pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> SetNewData -> Запрос неожиданно вернул пустоту. ")
		return
	}
	if err != nil {
		zapLog.Sug.Warnf("mails -> SetNewData -> Ошибка запроса. Причина: %s", err)
		return
	}

	return
}

func SetNewInfo(dataID int, statusName string, sendTo []string) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"INSERT INTO mails_info (msg_id, status_id, send_to) VALUES ($1, (SELECT id FROM statuses WHERE name = $2), $3)",
		dataID, statusName, pq.Array(sendTo))

	err = row.Scan()
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> SetNewInfo -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}

func SaveSendResult(id int, status, errorMsg string) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"UPDATE mails_info i SET "+
			"status_id = (Select id FROM statuses WHERE name = $2), "+
			"error_msg = $3, "+
			"date_send = now() "+
			"WHERE id = $1",
		id, status, errorMsg)

	err = row.Scan()
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> SaveSendResult -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}

func GetRepetitions(orderCode string, recipient []string) (id int, data string, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	row := pg.Pool.QueryRow(pg.Context,
		"SELECT d.id, d.data FROM mails_info i "+
			"LEFT JOIN mails_data d ON i.msg_id = d.id "+
			"WHERE d.order_code = ($1) and i.send_to = ($2) and i.status_id = (select id from statuses where name = ($3))",
		orderCode, recipient, "CONFIRMATION_AWAITING")

	err = row.Scan(&id, &data)

	return
}

func UpdateData(id int, data interface{}) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"UPDATE mails_data SET "+
			"data = $2 "+
			"WHERE id = $1",
		id, data)

	err = row.Scan()
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> UpdateData -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}

func RemoveMail(id int) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	_, err = pg.Pool.Exec(pg.Context, "delete from mails_data where id = $1", id)

	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("mails -> RemoveMail -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}
