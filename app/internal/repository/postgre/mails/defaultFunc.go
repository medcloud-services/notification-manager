package mails

import "gitlab.com/medcloud-services/notification-manager/app/internal/models"

func combine(m *models.OrderOutput, c models.OrderDB) {
	m.Id = c.Id
	m.Code = c.Code
	m.Name = c.Name
	m.Surname = c.Surname
	m.Birthday = c.Birthday
	m.Patronymic = c.Patronymic
	m.MedCenterId = c.MedCenterId
	m.State = c.State
	m.DoneTime = c.DoneTime
	m.PaymentTypeId = c.PaymentTypeId
	m.PaymentTypeChangeTime = c.PaymentTypeChangeTime
}
