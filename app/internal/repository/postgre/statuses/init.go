package statuses

import (
	"github.com/jackc/pgx/v4"
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func GetIdByName(name string) (id int, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	row := pg.Pool.QueryRow(pg.Context,
		"SELECT s.id FROM statuses s WHERE s.name = $1",
		name)

	id = 6 // todo Magic Number - Статус "Статус не найден"
	err = row.Scan(&id)
	if err == pgx.ErrNoRows {
		zapLog.Sug.Warnf("statuses -> GetIdByName -> Запрос неожиданно вернул пустоту для значения: %s", name)
		return
	}

	if err != nil {
		zapLog.Sug.Warnf("statuses -> GetIdByName -> Ошибка запроса. Причина: %s", err)
		return
	}

	return
}
