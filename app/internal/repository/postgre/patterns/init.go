package patterns

import (
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/support/postgre"

	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/jackc/pgx/v4"
)

func GetID(name string) (id int, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	row := pg.Pool.QueryRow(pg.Context,
		"SELECT id FROM patterns WHERE name=$1",
		name)

	err = row.Scan(&id)
	if err == pgx.ErrNoRows {
		zapLog.Sug.Warnf("patterns -> GetID -> Запрос неожиданно вернул пустоту. ")
		return
	}

	if err != nil {
		zapLog.Sug.Warnf("patterns -> GetID -> Ошибка запроса. Причина: %s", err)
		return
	}

	return
}
