package config

import (
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/support/postgre"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/jackc/pgx/v4"
)

func Get(misCode string) (data models.Config, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	row := pg.Pool.QueryRow(pg.Context,
		"SELECT data FROM config WHERE mis_code=$1",
		misCode)

	err = row.Scan(&data)

	return
}

func GetAll() (data map[string]models.MisConfig, err error) {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return
	}

	rows, err := pg.Pool.Query(pg.Context, "SELECT data, mis_code FROM config")
	if err != nil {
		zapLog.Sug.Errorf("config -> GetAll -> conn.Query failed. Причина: %s", err)
		return
	}
	defer rows.Close()

	data = make(map[string]models.MisConfig)
	for rows.Next() {
		var misConf models.MisConfig

		err = rows.Scan(&misConf.Conf, &misConf.Mis)
		if err != nil {
			zapLog.Sug.Warnf("config -> GetAll -> rows.Scan failed. Причина: %s", err)
		}
		data[misConf.Mis] = misConf
	}

	return
}

func Set(confData models.Config, misCode string) error {
	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"UPDATE config SET data = $1 where mis_code = $2",
		confData, misCode)

	err = row.Scan()
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("confData -> Set -> Ошибка запроса. Причина: %s", err)
		return err
	}

	return nil
}

func CreateNew(misCode string) (models.Config, error) {
	zapLog.Sug.Infof("config -> createNew -> Создаем новый экземпляр настроек. ")

	// default
	data := models.Config{
		SendPerMinute: 20,
	}

	cnf := config.Get()

	pg, err := postgre.Init(&cnf.Database)
	if err != nil {
		return data, err
	}

	row := pg.Pool.QueryRow(pg.Context,
		"INSERT INTO config (mis_code, data) VALUES ($1, $2)",
		misCode, data)

	err = row.Scan()
	if err != nil && err != pgx.ErrNoRows {
		zapLog.Sug.Warnf("config -> createNew -> Ошибка запроса. Причина: %s", err)
		return data, err
	}

	return data, nil
}
