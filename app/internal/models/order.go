package models

type OrderInput struct {
	Id                    int    `json:"mc_order_id"`
	Code                  string `json:"code"`
	Name                  string `json:"name"`
	Surname               string `json:"surname"`
	Birthday              string `json:"birthday"`
	Patronymic            string `json:"patronymic"`
	MedCenterId           int    `json:"medical_center_id"`
	State                 string `json:"state"`
	DoneTime              string `json:"done_timestamp"`
	PaymentTypeId         int    `json:"payment_type_id"`
	PaymentTypeChangeTime string `json:"payment_type_change_timestamp"`
}

type OrderOutput struct {
	Id                    int    `json:"McOrderId"`
	Code                  string `json:"Code"`
	Name                  string `json:"Name"`
	Surname               string `json:"Surname"`
	Birthday              string `json:"Birthday"`
	Patronymic            string `json:"Patronymic"`
	MedCenterId           int    `json:"MedicalCenterId"`
	State                 string `json:"State"`
	DoneTime              string `json:"DoneTimestamp"`
	PaymentTypeId         int    `json:"PaymentTypeId"`
	PaymentTypeChangeTime string `json:"PaymentTypeChangeTimestamp"`
}

type OrderDB struct {
	Id                    int    `json:"id"`
	Code                  string `json:"code"`
	Name                  string `json:"name"`
	Surname               string `json:"surname"`
	Birthday              string `json:"birthday"`
	Patronymic            string `json:"patronymic"`
	MedCenterId           int    `json:"medical_center_id"`
	State                 string `json:"state"`
	DoneTime              string `json:"done_time"`
	PaymentTypeId         int    `json:"payment_type_id"`
	PaymentTypeChangeTime string `json:"payment_type_change_time"`
}
