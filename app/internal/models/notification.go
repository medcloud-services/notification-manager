package models

type Msg struct {
	MisCode     string   `json:"misCode"`
	KeyCode     string   `json:"keyCode"`
	OrderCode   string   `json:"orderCode"`
	Data        string   `json:"data"`
	TypeMsg     string   `json:"typeMsg"`
	TypePattern string   `json:"typePattern"`
	SendTo      []string `json:"sendTo"`
}

type MsgResult struct {
	Id       int    `json:"notificationId"`
	Type     string `json:"type"`
	Status   string `json:"status"`
	ErrorMsg string `json:"errorText"`
}

type MailFilter struct {
	Status      string   `json:"status"`
	OrderNumber string   `json:"orderNumber"`
	Recipient   string   `json:"recepient"`
	CreateDate  []string `json:"createDate"`
	DoneDate    []string `json:"doneDate"`
	PayedDate   []string `json:"payedDate"`
	FullName    string   `json:"fullName"`

	Limit  int `json:"limit"`
	Offset int `json:"offset"`
}
