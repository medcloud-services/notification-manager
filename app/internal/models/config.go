package models

type MisConfig struct {
	Mis  string
	Conf Config
}

type Config struct {
	SendPerMinute    int  `json:"sendPerMinute"`
	ManualSendResult bool `json:"manualSendResult"`
}
