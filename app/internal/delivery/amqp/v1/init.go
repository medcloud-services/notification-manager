package v1

import (
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1/notificationListener"
	orderUpdates "gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1/orderListener"
)

func Init() error {
	// Запустить очереди нотификаций
	if err := notificationListener.InitListener(); err != nil {
		return err
	}

	// Запустить очереди заказов
	if err := orderUpdates.InitListener(); err != nil {
		return err
	}

	return nil
}
