package newNotification

import (
	"encoding/json"

	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/rabbitMQ"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/streadway/amqp"
)

const newNotificationQueue = "NewNotificationMsg"

func InitListener() bool {
	zapLog.Sug.Infof("newMsgListener -> InitListener -> Init connection to Rabbit. ")
	cnf := config.Get()

	rabbit, err := rabbitMQ.Init(&cnf.Rabbit)
	if err != nil {
		zapLog.Sug.Errorf("newMsgListener -> InitListener -> Error init connection to Rabbit: %s", err.Error())
		return false
	}

	if rabbit.IsConnExist() {
		go runNewNotificationListener(rabbit)
		return true
	}

	return false
}

func runNewNotificationListener(r *rabbitMQ.Rabbit) {
	zapLog.Sug.Infof("newMsgListener -> runNewNotificationListener. Старт прослушивания")

	err := r.StartListener(newNotificationQueue,
		func(delivery amqp.Delivery) {
			zapLog.Sug.Infof("notificationListener -> Listener. Получил новое сообщение")

			msg := models.Msg{}
			err := json.Unmarshal(delivery.Body, &msg)
			if err != nil {
				zapLog.Sug.Errorf("notificationListener -> Listener. Unmarshal OrderResult err: %s", err)
			}

			// Обработка сообщения
			notification.InitNotification(msg)
		}, "Отвечает за доставку новых сообщение в NotificationManager.", InitListener,
	)
	if err != nil {
		zapLog.Sug.Errorf("notificationListener -> Don't start Listener: %s", err)
	}
}
