package notificationListener

import (
	"errors"

	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1/notificationListener/newNotification"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1/notificationListener/resultSend"
)

func InitListener() error {
	if !newNotification.InitListener() {
		return errors.New("newNotificationListener don't start")
	}

	if !resultSend.InitListener() {
		return errors.New("resultSendListener don't start")
	}

	return nil
}
