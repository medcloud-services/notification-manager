package resultSend

import (
	"encoding/json"

	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/rabbitMQ"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/streadway/amqp"
)

const resultSendQueue = "NotificationSendResult"

func InitListener() bool {
	zapLog.Sug.Infof("resultSendListener -> InitListener -> Init connection to Rabbit. ")
	cnf := config.Get()

	rabbit, err := rabbitMQ.Init(&cnf.Rabbit)
	if err != nil {
		zapLog.Sug.Errorf("NotificationListener -> InitListener -> Error init connection to Rabbit: %s", err.Error())
		return false
	}

	if rabbit.IsConnExist() {
		go runResultSendListener(rabbit)
		return true
	}

	return false
}

func runResultSendListener(r *rabbitMQ.Rabbit) {
	zapLog.Sug.Infof("notificationListener -> runResultSendListener. Старт прослушивания")

	err := r.StartListener(resultSendQueue,
		func(delivery amqp.Delivery) {
			zapLog.Sug.Infof("notificationListener -> runResultSendListener. Получил сообщение: %s", delivery.Body)

			msg := models.MsgResult{}
			err := json.Unmarshal(delivery.Body, &msg)
			if err != nil {
				zapLog.Sug.Errorf("notificationListener -> runResultSendListener. Unmarshal OrderResult err: %s", err)
			}

			// Обработка сообщения
			notification.InitResult(msg)
		}, "Отвечает за получение результата отправки из mis в NotificationManager.", InitListener,
	)
	if err != nil {
		zapLog.Sug.Errorf("notificationListener -> Don't start runResultSendListener: %s", err)
	}
}
