package orderUpdates

import (
	"encoding/json"

	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/order"
	"gitlab.com/medcloud-services/notification-manager/app/pkg/rabbitMQ"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/streadway/amqp"
)

const orderUpdatesQueues = "OrderUpdates"

func InitListener() bool {
	zapLog.Sug.Infof("orderUpdates -> InitListener -> Init connection to Rabbit ")
	cnf := config.Get()

	rabbit, err := rabbitMQ.Init(&cnf.Rabbit)
	if err != nil {
		zapLog.Sug.Errorf("OrderUpdates -> InitListener -> Error init connection to Rabbit: %s", err.Error())
		return false
	}

	if rabbit.IsConnExist() {
		go startListener(rabbit)
		return true
	} else {
		zapLog.Sug.Warnf("orderUpdates -> InitListener -> Couldn't connect to Rabbit")
	}

	return false
}

func startListener(r *rabbitMQ.Rabbit) {
	zapLog.Sug.Infof("orderUpdates -> startListener. Старт прослушивания")

	err := r.StartListener(orderUpdatesQueues,
		func(delivery amqp.Delivery) {
			zapLog.Sug.Debugf("orderUpdates -> Listener Получил сообщение")

			orderResult := models.OrderInput{}
			err := json.Unmarshal(delivery.Body, &orderResult)
			if err != nil {
				zapLog.Sug.Errorf("orderUpdates -> Listener. Unmarshal OrderResult err: %s", err)
			}

			_ = order.Save(orderResult)
		}, "", InitListener,
	)
	if err != nil {
		zapLog.Sug.Errorf("orderUpdates -> Don't start Listener: %s", err)
	}
}
