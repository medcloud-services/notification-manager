package orderUpdates

import (
	"errors"

	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1/orderListener/orderUpdates"
)

func InitListener() error {
	if !orderUpdates.InitListener() {
		return errors.New("orderUpdatesListener don't start")
	}

	return nil
}
