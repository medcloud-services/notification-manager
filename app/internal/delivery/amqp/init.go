package amqp

import v1 "gitlab.com/medcloud-services/notification-manager/app/internal/delivery/amqp/v1"

func Init() error {
	if err := v1.Init(); err != nil {
		return err
	}

	return nil
}
