package config

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/medcloud-services/notification-manager/app/internal/service/notification/mail/periodicSender"

	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http/v1/api/response"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/service/configs"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/gin-gonic/gin"
)

func Get(c *gin.Context) {
	zapLog.Sug.Infof("conf -> Get()")

	conf, err := configs.Get(c.GetHeader("App-Code"))
	if err == configs.ErrConfigNotFound {
		conf, err = configs.Create(c.GetHeader("App-Code"))

		p := periodicSender.New(models.MisConfig{Mis: c.GetHeader("App-Code"), Conf: conf})
		p.Run()
	}

	if err != nil {
		zapLog.Sug.Errorf("conf -> Get() -> configs.Get error: %s", err.Error())
		response.Error(c, "configs.Get error: "+err.Error())
		return
	}

	response.Success(c, conf)
}

func Update(c *gin.Context) {
	zapLog.Sug.Infof("Msg -> Update()")

	jsonData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		zapLog.Sug.Errorf("conf -> Update(), ioutil.ReadAll err: %s", err.Error())
		response.Error(c, "Ошибка получение данных. Причина: "+err.Error())
		return
	}

	var cfg models.Config
	if err := json.Unmarshal(jsonData, &cfg); err != nil {
		zapLog.Sug.Errorf("conf -> Update(), json.Unmarshal err: %s", err.Error())
		response.Error(c, "Некорректные данные. Причина: "+err.Error())
		return
	}

	if cfg.SendPerMinute <= 0 {
		zapLog.Sug.Warnf("conf -> Update(), cfg.SendPerMinute <= 0")
		response.Error(c, "Некорректные данные. Причина: Количество отправляемых писем в минуту не может быть меньше одного.")
		return
	}

	misConfig := models.MisConfig{
		Mis:  c.GetHeader("App-Code"),
		Conf: cfg,
	}

	if err := configs.Set(misConfig); err != nil {
		zapLog.Sug.Errorf("conf -> Update(), config.Set err: %s", err.Error())
		response.Error(c, "Ошибка сохранения настроек. Причина: "+err.Error())
		return
	}

	p, err := periodicSender.Get(misConfig)
	if err == nil {
		p.SetConfig(misConfig.Conf)
		p.Restart()
	} else {
		p = periodicSender.New(misConfig)
		p.Run()
	}

	response.Success(c, nil)
}
