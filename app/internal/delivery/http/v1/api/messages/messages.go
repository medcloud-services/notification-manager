package messages

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http/v1/api/response"
	"gitlab.com/medcloud-services/notification-manager/app/internal/models"
	"gitlab.com/medcloud-services/notification-manager/app/internal/repository/postgre/mails"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"

	"github.com/gin-gonic/gin"
)

func Get(c *gin.Context) {
	zapLog.Sug.Infof("Msg -> Get()")
}

func GetMany(c *gin.Context) {
	zapLog.Sug.Infof("Msg -> GetMany()")

	jsonData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		zapLog.Sug.Errorf("Messages -> GetMany(), ioutil.ReadAll err: %s", err.Error())
		response.Error(c, "Ошибка получение данных. Причина: "+err.Error())
		return
	}

	var data struct {
		Type string `json:"type"`
	}

	if err := json.Unmarshal(jsonData, &data); err != nil {
		zapLog.Sug.Errorf("Messages -> GetMany(), json.Unmarshal err: %s", err.Error())
		response.Error(c, "Некорректные данные. Причина: "+err.Error())
		return
	}

	var res []interface{}

	switch data.Type {
	case "mail":
		var filter models.MailFilter
		if err := json.Unmarshal(jsonData, &filter); err != nil {
			zapLog.Sug.Errorf("Messages -> GetMany() -> mail filter, json.Unmarshal err: %s", err.Error())
			response.Error(c, "Некорректные данные. Причина: "+err.Error())
			return
		}

		res, err = mails.GetMails(filter, c.GetHeader("App-Code"))
		if err != nil {
			zapLog.Sug.Errorf("Messages -> GetMany() -> mails.GetMails return err: %s", err.Error())
			response.Error(c, "Ошибка запроса. Причина: "+err.Error())
		}
	default:
		zapLog.Sug.Warnf("Messages -> GetMany() -> Don't supported request type: %s", data.Type)
		response.Error(c, "Don't supported request type: "+data.Type)
	}

	zapLog.Sug.Info("Msg -> GetMany(). Count Result: ", len(res), " MisCode: "+c.GetHeader("App-Code"))

	response.Success(c, res)
}

func UpdateStatus(c *gin.Context) {
	zapLog.Sug.Infof("Msg -> UpdateStatusByStatusName()")

	jsonData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		zapLog.Sug.Errorf("conf -> Update(), ioutil.ReadAll err: %s", err.Error())
		response.Error(c, "Ошибка получение данных. Причина: "+err.Error())
		return
	}

	var data struct {
		Ids       []int  `json:"ids"`
		NewStatus string `json:"newStatus"`
	}
	if err := json.Unmarshal(jsonData, &data); err != nil {
		zapLog.Sug.Errorf("conf -> Update(), json.Unmarshal err: %s", err.Error())
		response.Error(c, "Некорректные данные. Причина: "+err.Error())
		return
	}

	// Обновить статусы для группы писем по ID
	updateRowsID, _ := mails.UpdateStatusByID(data.Ids, data.NewStatus, c.GetHeader("App-Code"))

	res := struct {
		UpdateCountRows int
	}{len(updateRowsID)}

	response.Success(c, res)
}

func SendAll(c *gin.Context) {
	zapLog.Sug.Infof("Msg -> SendAll()")

	// Помечаем к отправке все ожидающие письма
	updateRowsID, _ := mails.UpdateStatusByStatusName("CONFIRMATION_AWAITING", "READY_FOR_SEND", c.GetHeader("App-Code"))

	res := struct {
		CountToSend int
	}{len(updateRowsID)}

	response.Success(c, res)
}
