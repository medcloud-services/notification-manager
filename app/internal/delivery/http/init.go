package http

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/medcloud-services/notification-manager/app/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http/routes"
	zapLog "gitlab.com/medcloud-services/support/zap-logger"
)

func Start() error {
	cnf := config.Get()

	gin.SetMode(cnf.Server.Mode)

	// Объявляем роуты
	rts := routes.InitRoutes()

	// подключение документации обходится в: 18мб веса приложения и 3-4 доп потока.
	rts.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Запуск сервера
	zapLog.Sug.Infof("nm -> http -> Start server")
	if err := rts.Run(cnf.Server.Host + ":" + cnf.Server.Port); err != nil {
		zapLog.Sug.Errorf("gw -> http -> Error start server: %s", err.Error())
		return err
	}

	return nil
}
