package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http/v1/api/config"
	"gitlab.com/medcloud-services/notification-manager/app/internal/delivery/http/v1/api/messages"
)

func InitRoutes() *gin.Engine {
	r := gin.Default()

	groupV1 := r.Group("/api/v1")
	{

		conf := groupV1.Group("/config")
		{
			conf.GET("/get", config.Get)       // получить конфиг
			conf.PUT("/update", config.Update) // обновить конфиг
		}

		msg := groupV1.Group("/messages")
		{
			msg.GET("/get", messages.Get)                    // получить сообщение
			msg.POST("/get-many", messages.GetMany)          // получить набор сообщений
			msg.PUT("/update-status", messages.UpdateStatus) // обновить статус выбранного сообщения. (отменить, повторить)

			msg.GET("/send-all", messages.SendAll) // отправить все ожидающие сообщения
		}

	}
	// Пример по мульти-группа для нескольких версий апи https://stackoverflow.com/questions/42373423/how-to-add-multiple-groups-to-gin-routing-for-api-version-inheritance

	return r
}
