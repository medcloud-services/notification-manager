-- Write your migrate up statements here
create table statuses
(
    id   serial
        constraint statuses_pk
            primary key,
    name text not null,
    description text not null
);

comment on table statuses is 'Перечень статусов нотификаций';

comment on column statuses.name is 'Наименование статуса';

comment on column statuses.description is 'Описание статуса на русском';

INSERT INTO public.statuses (name, description) VALUES ('CONFIRMATION_AWAITING', 'Ожидает подтверждения.');
INSERT INTO public.statuses (name, description) VALUES ('READY_FOR_SEND', 'Готово к отправке');
INSERT INTO public.statuses (name, description) VALUES ('SENDING', 'Отправка. Сообщение доставлено в Rabbit.');
INSERT INTO public.statuses (name, description) VALUES ('SENDED', 'Отправлено.');
INSERT INTO public.statuses (name, description) VALUES ('ERROR', 'Ошибка.');
INSERT INTO public.statuses (name, description) VALUES ('UNDEFINED', 'Запрашиваемый статус не существует.');
INSERT INTO public.statuses (name, description) VALUES ('QUEUE_FOR_SENDING', 'Забрано отправщиком сообщений. Сообщение ожидает своей очереди перед доставкой в Rabbit.');

---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table statuses;