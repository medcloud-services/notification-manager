
ALTER TABLE public.task_queue
    ALTER COLUMN send_time DROP NOT NULL;


ALTER TABLE public.task_queue
    ADD COLUMN medical_center_id integer NOT NULL DEFAULT 1;