-- Write your migrate up statements here
create table patterns
(
    id   serial
        constraint patterns_pk
            primary key,
    data text not null default '',
    name text not null
);

comment on table patterns is 'Таблица шаблонов для построение нотификаций';

comment on column patterns.data is 'Шаблон (в любом виде)';

comment on column patterns.name is 'Наименование шаблона';


INSERT INTO public.patterns (data, name) VALUES (DEFAULT, 'orderResult');

---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table patterns;