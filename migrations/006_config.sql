-- Write your migrate up statements here
create table config
(
    id serial
        constraint config_pk
            primary key,
    mis_code text not null,
    data jsonb default '{}' not null
);

comment on table config is 'Таблица хранит настройки каждого потребителя сервиса';

comment on column config.mis_code is 'Уникальный код потребителя';

comment on column config.data is 'Данные настроек';

create unique index config_mis_code_uindex
    on config (mis_code);


---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table config;