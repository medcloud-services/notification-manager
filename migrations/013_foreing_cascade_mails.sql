-- Write your migrate up statements here
alter table mails_info
    add constraint mails_info_mails_data_id_fk
        foreign key (msg_id) references mails_data
            on delete cascade;

---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

alter table mails_info drop constraint mails_info_mails_data_id_fk;