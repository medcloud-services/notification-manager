-- Write your migrate up statements here
create table mails_info
(
    id        serial,
    msg_id    int,
    status_id int,
    send_to   text[] not null,
    date_send timestamp,
    error_msg text not null default ''
);

comment on table mails_info is 'Хранит информация об отправке для нотификаций: Email';

comment on column mails_info.msg_id is 'ID сообщения из таблицы mails_data ';

comment on column mails_info.status_id is 'ID статуса из таблицы statuses ';

comment on column mails_info.send_to is 'Отправить на: (почта/номер)';

comment on column mails_info.date_send is 'Дата отправки';

comment on column mails_info.error_msg is 'Текст ошибки. (в случае ошибки отправки)';

alter table mails_info
    add constraint msg_mails_pk
        primary key (id);

--alter table mails_info alter column error_msg set default '';
---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table mails_info;