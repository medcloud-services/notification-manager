-- Write your migrate up statements here
create table mails_data
(
    id serial,
    mis_code text not null,
    key_code text not null,
    order_code text,
    data jsonb,
    pattern_id int not null,
    created_at timestamp default current_timestamp not null
);

comment on column mails_data.pattern_id is 'ID шаблона для построения нотификации. Из таблицы patterns';

comment on table mails_data is 'Хранит основные данные для нотификаций: Email';

comment on column mails_data.mis_code is 'Уникальный код потребителя';

comment on column mails_data.key_code is 'Уникальный код сообщения';

comment on column mails_data.order_code is 'Уникальный код ордера. Связан с полем order в таблице clone_date_order';

comment on column mails_data.data is 'Данные сообщения в JSON ';

comment on column mails_data.created_at is 'Дата создания сообщения ';

alter table mails_data
    add constraint mails_data_pk
        primary key (id);


---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table mails_data;