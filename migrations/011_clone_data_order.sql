-- Write your migrate up statements here
create table clone_data_order
(
    id int,
    code text,
    name text,
    surname text,
    patronymic text,
    birthday date,
    medical_center_id int,
    state text,
    done_time timestamp,
    payment_type_id int,
    payment_type_change_time timestamp
);

alter table clone_data_order
    add constraint clone_data_order_pk
        primary key (code);

comment on table clone_data_order is 'Копия данных о заказе для фильтраций. ';

comment on column clone_data_order.id is 'order id in Mis';

comment on column clone_data_order.code is 'orderCode';

comment on column clone_data_order.patronymic is 'Отчество';


---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop table clone_data_order;