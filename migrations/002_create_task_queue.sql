CREATE SEQUENCE task_queue_id_seq;


CREATE TABLE public.task_queue
(
    id integer NOT NULL DEFAULT nextval('task_queue_id_seq'::regclass),
    data json NOT NULL,
    task_key character varying(255) COLLATE pg_catalog."default" NOT NULL,
    send_time timestamp without time zone NOT NULL,
    is_sent boolean NOT NULL,
    queue_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT task_queue_pkey PRIMARY KEY (id)
);


COMMENT ON COLUMN public.task_queue.data
    IS 'данные в формате json';

COMMENT ON COLUMN public.task_queue.task_key
    IS 'Уникальный ключ задачи';

COMMENT ON COLUMN public.task_queue.send_time
    IS 'Время отправки (если задача выполняется с задержкой)';

COMMENT ON COLUMN public.task_queue.is_sent
    IS 'Было отправлено на выполнение';

COMMENT ON COLUMN public.task_queue.queue_name
    IS 'Наименование очереди';