-- Write your migrate up statements here
drop function if exists get_mails;

CREATE OR REPLACE function get_mails(createAt text[], doneAt text[], payAt text[], fullName text, orderCode text,
                                     recipient text, statusName text, misCode text, limitR int,
                                     offsetR int)
    returns TABLE
            (
                id        int,
                data      jsonb,
                send_to   text[],
                create_at text,
                date_send text,
                name      text,
                "order"   jsonb,
                error_msg text
            )
as
$$
DECLARE
    _fio text[];
begin
    SELECT regexp_split_to_array(fullName, '\s+') INTO _fio;

    RETURN QUERY (
        SELECT d.id,
               d.data,
               i.send_to,
               text(d.created_at),
               text(i.date_send),
               s.name,
               to_jsonb(c),
               i.error_msg
        FROM mails_info i
                 LEFT JOIN mails_data d ON i.msg_id = d.id
                 LEFT JOIN statuses s on i.status_id = s.id
                 LEFT JOIN clone_data_order c on d.order_code = c.code
        WHERE d.mis_code = misCode
          AND (statusName = '' OR s.name = statusName)
          AND (recipient = '' OR text(i.send_to) LIKE '%' || recipient || '%')
          AND (orderCode = '' OR d.order_code LIKE orderCode || '%')
          AND (_fio[1] = '' OR c.name LIKE _fio[1] || '%' OR c.surname LIKE _fio[1] || '%' OR
               c.patronymic LIKE _fio[1] || '%')
          AND (_fio[2] IS NULL OR c.name LIKE _fio[2] || '%' OR c.surname LIKE _fio[2] || '%' OR
               c.patronymic LIKE _fio[2] || '%')
          AND (_fio[3] IS NULL OR c.name LIKE _fio[3] || '%' OR c.surname LIKE _fio[3] || '%' OR
               c.patronymic LIKE _fio[3] || '%')
          AND (createAt[1] IS NULL OR d.created_at > date(createAt[1]))
          AND (createAt[2] IS NULL OR d.created_at < date(createAt[2]))
          AND (doneAt[1] IS NULL OR d.created_at > date(doneAt[1]))
          AND (doneAt[2] IS NULL OR d.created_at < date(doneAt[2]))
          AND (payAt[1] IS NULL OR d.created_at > date(payAt[1]))
          AND (payAt[2] IS NULL OR d.created_at < date(payAt[2]))
        GROUP BY i.send_to, d.data, d.id, d.created_at, i.date_send, s.name, c.*, i.error_msg
        ORDER BY d.id DESC
        LIMIT limitR OFFSET offsetR
    );

end
$$
    language plpgsql;
---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop function if exists get_mails;