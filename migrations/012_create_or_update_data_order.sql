-- Write your migrate up statements here
drop function if exists create_or_update_data_order;

CREATE OR REPLACE function create_or_update_data_order(id int, code text, name text, surname text,
                                                       birthday date, patronymic text, medical_center_id int,
                                                       state text,
                                                       done_time timestamp, payment_type_id int,
                                                       payment_type_change_time timestamp)
    returns bool
as
$$
declare
    isExists bool;
begin
    SELECT True from clone_data_order as c where c.code = $2 into isExists;

    IF isExists THEN
        UPDATE clone_data_order c
        SET id                       = $1,
            name                     = $3,
            surname                  = $4,
            birthday                 = $5,
            patronymic               = $6,
            medical_center_id        = $7,
            state                    = $8,
            done_time                = $9,
            payment_type_id          = $10,
            payment_type_change_time = $11
        WHERE c.code = $2;
    ELSE
        INSERT into clone_data_order(id, code, name, surname, birthday, patronymic,
                                     medical_center_id, state, done_time, payment_type_id, payment_type_change_time)
        values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);
    end if;

    return isExists;
end
$$
    language plpgsql;

---- create above / drop below ----

-- Write your migrate down statements here. If this migration is irreversible
-- Then delete the separator line above.

drop function if exists create_or_update_data_order;