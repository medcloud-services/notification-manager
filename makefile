run:
	docker-compose up  --remove-orphans --build

run_nm:
	go run -race cmd/nm/main.go

build_nm:
	env GOOS=linux go build gitlab.com/medcloud-services/notification-manager/cmd/nm

lint:
	gofumpt -l -w -s ./notification-manager/..
	golangci-lint run --fix