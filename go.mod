module gitlab.com/medcloud-services/notification-manager

go 1.16

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.6.1
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-gonic/gin v1.4.0
	github.com/jackc/pgtype v1.8.1
	github.com/jackc/pgx/v4 v4.13.0
	github.com/lib/pq v1.10.2 // indirect
	github.com/spf13/viper v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.7.0
	gitlab.com/medcloud-services/support/postgre v1.0.0
	gitlab.com/medcloud-services/support/zap-logger v1.1.0
	go.uber.org/zap v1.19.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
